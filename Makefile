CPP_BASE = $(wildcard base/*.cpp)
CPP_SU_N = $(wildcard su_n/*.cpp)

OBJECTS = $(CPP_BASE:.cpp=.o) $(CPP_SU_N:.cpp=.o)

TARGETS_SU_N = $(basename $(wildcard su_*.cpp))
# TARGETS_SU_N = su_n_hubhei_quench su_n_hubhei_peierls su_n_test

# -DARMA_DONT_USE_WRAPPER -DARMA_USE_BLAS -DARMA_USE_LAPACK -DARMA_USE_HDF5 -DFTWA_CACHE_CHECKPOINTS
CXX_heisenbyte = g++
CXXFLAGS_heisenbyte = -Wall -std=c++14 -march=native -O2 -fopenmp -DFTWA_OMIT_U -DARMA_DONT_USE_WRAPPER -DFTWA_WITH_TIMER#-DARMA_NO_DEBUG# -DFTWA_CACHE_CHECKPOINTS 
LDFLAGS_heisenbyte = -lopenblas -llapack -lsqlite3 -lhdf5_serial -lboost_program_options

CXX_ds10 = icpc
CXXFLAGS_ds10 = -Wall -std=c++14 -xHost -O2 -I/net/theorie/auto/scratch1/osterkorn/ds10/include -I${MKLROOT}/include -DARMA_ALLOW_FAKE_GCC -DARMA_DONT_USE_WRAPPER -DARMA_NO_DEBUG# -DFTWA_WITH_TIMER# -DFTWA_CACHE_CHECKPOINTS 
# CXXFLAGS_ds10 = -Wall -std=c++14 -xHost -O2 -qopenmp -I/net/theorie/auto/scratch1/osterkorn/ds10/include -I${MKLROOT}/include -DARMA_ALLOW_FAKE_GCC -DARMA_DONT_USE_WRAPPER -DARMA_NO_DEBUG# -DFTWA_WITH_TIMER# -DFTWA_CACHE_CHECKPOINTS 
LDFLAGS_ds10 = -L/net/theorie/auto/scratch1/osterkorn/ds10/lib64 -lsqlite3 -lhdf5 -lboost_program_options -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_lp64.a ${MKLROOT}/lib/intel64/libmkl_sequential.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm -ldl
# LDFLAGS_ds10 = -L/net/theorie/auto/scratch1/osterkorn/ds10/lib64 -lsqlite3 -lhdf5 -lboost_program_options -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_lp64.a ${MKLROOT}/lib/intel64/libmkl_intel_thread.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -liomp5 -lpthread -lm -ldl
 
CXX_ds9 = icpc
CXXFLAGS_ds9 = -Wall -std=c++14 -march=native -O3
LDFLAGS_ds9 = -larmadillo -lsqlite3 -lhdf5 -lboost_program_options

# default values
CXX = g++
CXXFLAGS = -Wall -std=c++14 -march=native -O2 -I/opt/homebrew/include/ -DARMA_USE_HDF5 -DARMA_DONT_USE_WRAPPER -DFTWA_WITH_TIMER -DARMA_NO_DEBUG# -DFTWA_CACHE_CHECKPOINTS 
LDFLAGS = -L/opt/homebrew/lib/ -L/opt/homebrew/Cellar/openblas/0.3.24/lib -lopenblas -llapack -lsqlite3 -lhdf5 -lboost_program_options

hn := $(shell hostname | cut -d"." -f1)

ifdef CXX_$(hn)
echo "use CXX_$(hn)"
CXX = $(CXX_$(hn))
CXXFLAGS = $(CXXFLAGS_$(hn))
LDFLAGS = $(LDFLAGS_$(hn))
endif

all: $(TARGETS_SU_N)

su_%: su_%.cpp $(OBJECTS)
	$(CXX) $(CXXFLAGS) $< -o $@ $(OBJECTS) $(LDFLAGS)

# .PRECIOUS: %.o

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	rm -f $(TARGETS_SU_N) $(OBJECTS)
